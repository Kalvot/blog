<?php
declare(strict_types = 1); //sprawdzanie typów w PHP7
require_once 'database.php';
require_once 'Post.php';  

$PostsRepository = PostRepository::instance(); //globalny obiekt repo z postami

class PostRepository
{ //Singleton. Wzorzec tworzenia obiektu globalnego. Mozna poczytać na necie
    private static $instance;

    static function instance()
    {
        if (!isset(static::$instance)) {
            static::$instance = new PostRepository();
        }
        return static::$instance;
    }

    function save(string $title, string $content): int
    {
        global $db;
        $sql = $db->prepare("INSERT INTO Posts (title, content)
                VALUES (:title, :content)");
        $sql->bindParam(":title", $title);
        $sql->bindParam(":content", $content);
        try{
            $sql->execute();
            return $db->lastInsertId();
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
        
    }

    function get($id) : Post
    {
        global $db;
        $sql = "SELECT * FROM Posts WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $post  = $stmt->fetchAll()[0];
        return new Post($post['title'], $post['content'], $post['id']);
    }

    public function getAll(): array
    {
        global $db;

        $sql = "SELECT * FROM Posts";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $rows  = $stmt->fetchAll();
        $posts = [];
        foreach ($rows as $i => $row){
            $posts[$i] = new Post($row['title'], $row['content'], $row['id']);
        }
        return $posts;
    }
}