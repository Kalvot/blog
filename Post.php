<?php

declare(strict_types = 1); //sprawdzanie typów w PHP7


require_once 'PostRepository.php';

class Post {
    public $title;
    public $content;
    public $id;
    
    public function __construct($title, $content, $id) {
        $this->title = $title;
        $this->content = $content;
        $this->id = intval($id);
        
    }
    
    public function save(){
        return PostRepository::save($this->title, $this->content);
    }
}
