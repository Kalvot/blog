<?php

namespace App\Routing;

class Route
{
    protected $pattern;
    protected $controller;
    protected $action;
    protected $parameters;

    public function __construct($pattern, $controller, $action)
    {
        $this->pattern    = $pattern;
        $this->controller = $controller;
        $this->action     = $action;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getPattern()
    {
        return $this->pattern;
    }
}