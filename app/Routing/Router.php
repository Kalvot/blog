<?php

namespace App\Routing;

use App\Routing\Route;

class Router
{
    private $publicDir;
    private $url;
    private $routes = [];

    public function __construct($dir, $url)
    {
        $this->publicDir = $dir;
        $this->url       = $url;
    }

    public function addRoute(Route $route)
    {
        $this->routes[] = $route;
    }

    public function run()
    {
        
    }
}