<?php
include 'header.php';
include_once 'PostRepository.php';
?>

<h1>Strona główna</h1>
<a href="new.php">Dodaj nowy artykuł</a>
<br>
<?php
try {
    $posts = $PostsRepository->getAll();
    
    foreach ($posts as $post) {
        ?>
<a href="single.php?p=<?php echo $post->id;?>"><h3><?php echo $post->title; ?></h3></a>
        <p><?php echo $post->content; ?></p>
        <?php
    }
} catch (Exception $ex) {
    echo $ex->getMessage();
}

include 'footer.php';
